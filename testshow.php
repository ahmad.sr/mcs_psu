<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <title>Document</title>
</head>
<?php
include "dbconnect.php";
?>
<body>
    <center>
    <h1>แสดงข้อมูล</h1>
<table class='w3-table-all w3-card-4' style = 'width: 90%' >
<thead class="thead-dark">
    <tr> 
        <th>รหัสนักศึกษา</th>
        <th>ชื่อ-นามสกุล</th>
        <th>email</th>
        <th>รุ่นที่</th>
        <th>อาจารย์ที่ปรึกษา</th>
        
    </tr>
    <?php
        $sql = "SELECT * FROM student 
                JOIN advisor ON student.techID = advisor.techID
                where student.techID = 16 ORDER BY stuID 
                ";
        $result = $conn->query($sql);

        while($row = $result->fetch_assoc()) {
        
            echo "<tr>";
            echo "<td>".$row["stuID"]."</td>";        
            echo "<td>".$row["stuNme"]." ".$row["stuSurnme"]."</td>";
            echo "<td>".$row["email"]."</td>";        
            echo "<td>".$row["generation"]."</td>";
            echo "<td>".$row["techNme"]." ".$row["techSurnme"]."</td>";        
            echo "</tr>";	
        }
    ?>

</table>
</center>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<figure class="highcharts-figure">
    <div id="container007"></div>
    <p class="highcharts-description">
        Pie charts are very popular for showing a compact overview of a
        composition or comparison. While they can be harder to read than
        column charts, they remain a popular choice for small datasets.
    </p>
</figure>
<script>
    Highcharts.chart('container007', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Browser market shares in January, 2018'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            }
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [{
            name: 'Chrome',
            y: 61.41,
            sliced: true,
            selected: true
        }, {
            name: 'Internet Explorer',
            y: 11.84
        }, {
            name: 'Firefox',
            y: 10.85
        }, {
            name: 'Edge',
            y: 4.67
        }, {
            name: 'Safari',
            y: 4.18
        }, {
            name: 'Sogou Explorer',
            y: 1.64
        }, {
            name: 'Opera',
            y: 1.6
        }, {
            name: 'QQ',
            y: 1.2
        }, {
            name: 'Other',
            y: 2.61
        }]
    }]
});
</script>

<script src="https://code.highcharts.com/modules/data.js"></script>
<figure class="highcharts-figure">
    <div id="container"></div>
    <br><hr><center>
<table id="datatable" class='w3-table-all w3-card-4' style = 'width: 90%' >
        <thead>
            <tr>
                <th></th>
                <th>Jane</th>
                <th>John</th>
                <th>John</th>
                <th>John</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>Apples</th>
                <td>3</td>
                <td>4</td>
                <td>4</td>
                <td>4</td>
            </tr>
            <tr>
                <th>Pears</th>
                <td>2</td>
                <td>0</td>
            </tr>
            <tr>
                <th>Plums</th>
                <td>5</td>
                <td>11</td>
            </tr>
            <tr>
                <th>Bananas</th>
                <td>1</td>
                <td>1</td>
            </tr>
            <tr>
                <th>Oranges</th>
                <td>2</td>
                <td>4</td>
            </tr>
        </tbody>
    </table>
    </center>
</figure>

<script>
    Highcharts.chart('container', {
    data: {
        table: 'datatable'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: 'Data extracted from a HTML table in the page'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: 'Units'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});
</script>
    
</body>
</html>